# checkplace-be
chess spring boot application

# Compile and generare jar
```bash
gradle clean bootJar
```

# Generate docker image
```bash
gradle buildImage
```

