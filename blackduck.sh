#!/usr/bin/env sh
docker run \
    --rm \
    -v ${PWD}:/source \
    -v /build/output/files:/output \
    blackducksoftware/detect:7-gradle \
    --blackduck.url="$blackduck_url" \
    --blackduck.api.token="$blackduck_api_token" \
    --detect.excluded.directories=gradle/,.idea/,e2e/,venv/